import logging
import requests

sample_ids = [
    347140347, 686048400, 435608613, 886494815,
    249693334, 406502997, 158535134, 177713572,
    756274640, 456767165, 162052476, 825304400,
    320009267, 189965903, 699285638, 382392299,
    994022214, 759224212
]

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s') # noqa


for order_id in sample_ids:
    resp = requests.get(f"http://localhost:8080/{order_id}") # noqa
    logging.info(resp.json())
