import logging
import json

from aiohttp import web
from service import Data


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
routes = web.RouteTableDef()
database = Data()


@routes.get('/{id}')
async def get(request): # noqa
    try:
        status, data = await database.get(
            int(request.match_info['id'])
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


@routes.get('/{id}/{field}')
async def get(request):
    try:
        status, data = await database.get(
            int(request.match_info['id']),
            request.match_info['field']
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


@routes.post('/{id}/{field}')
async def post(request):
    try:
        logging.info('post request with field')
        body = await request.text()
        logging.info(body)
        status, data = await database.post(
            id=int(request.match_info['id']),
            body=body,
            field=request.match_info['field']
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


@routes.post('/{id}')
async def post(request):
    try:
        logging.info('post request with no field')
        try:
            body = await request.json()
        except json.decoder.JSONDecodeError:
            raise web.HTTPBadRequest()
        logging.info(body)
        status, data = await database.post(
            id=int(request.match_info['id']),
            body=body
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


@routes.patch('/{id}')
async def patch(request):
    try:
        logging.info('patch request with no field')
        try:
            body = await request.json()
        except json.decoder.JSONDecodeError:
            raise web.HTTPBadRequest()
        status, data = await database.patch(
            id=int(request.match_info['id']),
            body=body
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


@routes.patch('/{id}/{field}')
async def patch(request):
    try:
        logging.info('patch request with field')
        body = await request.text()
        logging.info(body)
        status, data = await database.patch(
            id=int(request.match_info['id']),
            body=body,
            field=request.match_info['field']
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


@routes.delete('/{id}')
async def delete(request):
    try:
        logging.info('delete request')
        status, data = await database.delete(
            id=int(request.match_info['id'])
        )
        return web.json_response(status=status, data=data)
    except web.HTTPException:
        raise


app = web.Application()
app.add_routes(routes)
web.run_app(app)
