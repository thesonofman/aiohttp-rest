import asyncio
import logging
import mmap

from aiohttp import web
from struct import *


class Data():
    HTTP_OK = 200

    def __init__(self):
        self.__source = open('resources/bytes_sample', 'r+b')
        self.__mm = mmap.mmap(self.__source.fileno(), 0)
        self.__fields = [
            'order_id',
            'region',
            'country',
            'item_type',
            'sold',
            'price'
        ]
        # self.__data = {669165933 : {'region' : 'Australia and Oceania',
        #                           'country' : 'Tuvalu',
        #                           'item_type' : 'Baby Food',
        #                           'sold' : 9925,
        #                           'price' : 255.28}}
        self.__mapping = {}
        self.__free_space = []
        index = 0
        while index < len(self.__mm):
            order_id = int(unpack('I', self.__mm[index:index + 4])[0])
            logging.info(order_id)
            if (order_id == 0):
                self.__free_space.append(index)
            else:
                self.__mapping[int(unpack('I', self.__mm[index:index + 4])[0])] = index # noqa
            index += 104
        logging.info(self.__free_space)

    def __del__(self):
        self.__source.close()

    async def map(order_id, region, country, item_type, sold, price): # noqa
        line = pack(
            'I35s35s20sif',
            int(order_id),
            str(region).encode('utf-8'),
            str(country).encode('utf-8'),
            str(item_type).encode('utf-8'),
            int(sold),
            float(price)
        )
        return line

    async def unmap(line):
        order_id, region, country, item_type, sold, price = tuple(unpack('I35s35s20sif', line)) # noqa
        return {
            'order_id': int(order_id),
            'region': region.decode("utf-8").strip('\x00'),
            'country': country.decode("utf-8").strip('\x00'),
            'item_type': item_type.decode("utf-8").strip('\x00'),
            'sold': int(sold),
            'price': float(price)
        }

    async def get(self, id, field=None):
        if id in self.__mapping.keys():
            index = self.__mapping[id]
        else:
            raise web.HTTPNotFound()
        if field is None:
            return Data.HTTP_OK, await Data.unmap(self.__mm[index:index + 104])
        else:
            if field in self.__fields:
                return Data.HTTP_OK, (await Data.unmap(self.__mm[index:index + 104]))[field] # noqa
            else:
                raise web.HTTPNotFound()

    async def post(self, id, body, field=None):
        if field is None:
            if id in self.__mapping.keys():
                raise web.HTTPConflict()
            else:
                # raise web.HTTPNotImplemented()
                return 418, None
        else:
            if id not in self.__mapping.keys():
                raise web.HTTPNotFound()
            index = self.__mapping[id]
            if field in self.__fields:
                line = await Data.unmap(self.__mm[index:index + 104])
                line[field] = body
                self.__mm[index:index + 104] = await Data.map(**line)
                return Data.HTTP_OK, line
            else:
                raise web.HTTPNotFound()

    async def patch(self, id, body, field=None):
        if id not in self.__mapping.keys():
            raise web.HTTPNotFound()
        index = self.__mapping[id]
        if field is None:
            line = await Data.unmap(self.__mm[index:index + 104])
            line.update(body)
            self.__mm[index:index + 104] = await Data.map(**line)
            return Data.HTTP_OK, line
        else:
            if field in self.__fields:
                line = await Data.unmap(self.__mm[index:index + 104])
                line[field] = body
                self.__mm[index:index + 104] = await Data.map(**line)
                return Data.HTTP_OK, line
            else:
                raise web.HTTPNotFound()

    async def delete(self, id):
        if id not in self.__mapping.keys():
            raise web.HTTPNotFound()
        index = self.__mapping[id]
        del self.__mapping[id]
        self.__mm[index:index + 4] = pack('I', 0)
        self.__free_space.append(index)
        logging.info(self.__free_space)
        return Data.HTTP_OK, None
